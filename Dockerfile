FROM mono:6

EXPOSE 42420

RUN  apt-get update && \
     apt-get install -y screen wget curl procps && \
     useradd -u 4000 -ms /bin/bash vintagestory && \
     mkdir /home/vintagestory/server && \
     mkdir -p /var/vintagestory/data && \
     chown vintagestory:vintagestory /home/vintagestory/server && \
     chown -R vintagestory:vintagestory /var/vintagestory

USER vintagestory:vintagestory
WORKDIR /home/vintagestory/server
VOLUME /var/vintagestory/data

ADD --chown=vintagestory:vintagestory https://cdn.vintagestory.at/gamefiles/stable/vs_server_1.15.9.tar.gz ./

RUN ["tar", "xzf", "vs_server_1.15.9.tar.gz"]

CMD ["mono", "VintagestoryServer.exe", "--dataPath", "/var/vintagestory/data"]
