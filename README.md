# Vintage Story Dockerfile

Minimalistic Dockerfile for a [Vintage Story](https://www.vintagestory.at/) server.

Remember to change the server version before building (no automation unless I am asked to build another server).
